/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tqsHW1.WeatherProject.Controllers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 *
 * @author jcps
 */
@Controller
@RequestMapping(path = "/")
public class ViewController {
    
    private String index = "index";
    
    @GetMapping(value = "index")
    public String returnIndex() {
            return index;
    }
    
}
